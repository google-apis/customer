package main

import (
	"fmt"
	"hash/fnv"

	"github.com/rs/xid"
)

func hash(s string) uint64 {
	h := fnv.New64a()
	h.Write([]byte(s))
	return h.Sum64()
}

func main() {
	guid := xid.New()
	fmt.Println("clientId:", guid.String())
	fmt.Println("key:", hash(guid.String()))
}
